# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

A Partition Widget or Kind of Multi Slider with multiple sliders and adjustable levels between them. Can be included inside a qmake or CMake based project. There can be various usage of this library. Right now it is being used for partitioning a timeline into disjoint segments. However it can also be used as gradient selector or who knows what purpose.

![Screenshot](PartitionView.png)

### How do I get set up? ###

    PartitionWidget w;
    w.show();

### Contribution guidelines ###

* Some code requires cleanup
* Some generializations required

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
