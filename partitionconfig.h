#ifndef PARTITIONCONFIG_H
#define PARTITIONCONFIG_H

#include <QWidget>

template <typename T>
struct domain{
    T b;
    T e;

    domain(const T& b, const T& e): b(b), e(e){}
    struct range{
        const domain<T>& _domain;

        T bp;
        T ep;

        range(const domain<T>& domain, const T& bp, const T& ep): _domain(domain), bp(bp), ep(ep){}
        T map(const T& v) const{
            double b = _domain.b;
            double e = _domain.e;
            double vp = (bp*e -b*ep -bp*v +ep*v)/(e-b);
            return vp;
        }
        T at(const T& v) const{
            return map(v);
        }
        T operator()(const T& v) const{
            return map(v);
        }
    };
    range to(const T& bp, const T& ep){
        return range(*this, bp, ep);
    }
};

/**
 * usage scale(10, 100).to(0, 1).at(50)
 * @tparam T
 * @param b
 * @param e
 * @return
 */
template <typename T>
domain<T> scale(const T& b, const T& e){
    return domain<T>(b, e);
}

class PartitionCursor;

/**
 * @brief Global configuration for all cursors and partitions
 */
struct Q_DECL_EXPORT PartitionConfig{
    enum Activity{
       NoActivity,
       RangeActivity,
       WeightActivity,
       ControlActivity,
       XValueActivity,
       YValueActivity
    };

    double width;
    double height;
    double stretch_min;
    double margin;
    double xmax;
    double ymax;

    Activity activity;
    PartitionCursor* focused;

    explicit PartitionConfig(QWidget* widget);

    double stretch() const;
    double canvash(double max=0) const;
    double canvasw() const;
    /**
     * @brief map the given value from [0, canvash] or [0, canvasw] to [0,1] depending on orientation
     * @return
     */
    double to_domain(double value, Qt::Orientation orientation) const;
    /**
     * @brief map the given value to [0, xmax] or [0, ymax] depending on orientation (uses to_domain internally)
     * @param value
     * @return
     */
    double to_range(double value, Qt::Orientation orientation) const;
  private:
    PartitionConfig(const PartitionConfig& other){}
};

#endif // PARTITIONCONFIG_H
