#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPaintEvent>
#include "partitionconfig.h"
#include "partitioncursor.h"
#include <iterator>

class PartitionWidget;

struct Q_DECL_EXPORT PartitionCursorIterator: public std::iterator<std::bidirectional_iterator_tag, const PartitionCursor>{
    friend class PartitionWidget;

    PartitionCursorIterator& operator++() {_cursor = _cursor->_next; return *this;}
    PartitionCursorIterator operator++(int) {PartitionCursorIterator retval = *this; ++(*this); return retval;}
    bool operator==(PartitionCursorIterator other) const {return _cursor == other._cursor;}
    bool operator!=(PartitionCursorIterator other) const {return !(*this == other);}
    reference operator*() const {return *_cursor;}
  private:
    explicit PartitionCursorIterator(const PartitionCursor* cursor) : _cursor(cursor) {}

    const PartitionCursor* _cursor;
};

class Q_DECL_EXPORT PartitionWidget : public QWidget{
  Q_OBJECT
  PartitionConfig _config;
  public:
    typedef PartitionCursorIterator Iterator;
  private:
    PartitionCursor* _first;
    PartitionCursor* _last;
    QVector<PartitionCursor*>  _cursors;
  public:
    explicit PartitionWidget(QWidget *parent = 0);
    ~PartitionWidget();
    void paintEvent(QPaintEvent* ev);
  public:
    double height() const  {return _config.height;}
    double width() const   {return _config.width;}
    double margin() const  {return _config.margin;}
    PartitionConfig::Activity activity() const {return _config.activity;}
    PartitionCursor* focused() const {return _config.focused;}
    double stretch() const {return _config.stretch();}
  private:
    void mousePressEvent(QMouseEvent* ev);
    void mouseReleaseEvent(QMouseEvent* ev);
    void mouseMoveEvent(QMouseEvent* ev);
    void mouseDoubleClickEvent(QMouseEvent* ev);
    void resizeEvent(QResizeEvent* ev);
    void keyReleaseEvent(QKeyEvent* ev);
  public slots:
    void setLimit(double value, Qt::Orientation orientation, bool cascade=true);
  public:
    Iterator begin() const {return Iterator(_first->_next);}
    Iterator end() const {return Iterator((PartitionCursor*)0x0);}
  public:
    void clear();
    void insert(unsigned index, double delta, double value=0.0f);
    void push(double delta, double value=0.0f);
    void setValue(unsigned index, double value);
};

#endif // WIDGET_H
