#include "partitioncursor.h"
#include <QDebug>

PartitionCursor::PartitionCursor(const PartitionConfig &config, Type type): _config(config), _type(type), _index(0), _shift(0), _weight(0.0f), _previous(0x0), _next(0x0), _activated(false), _selected(false), _yvalue(false){
    if(type == OpeningCursor){
        _shift = 0;
    }else if(type == ClosingCursor){
        _shift = width();
    }
}
PartitionCursor *PartitionCursor::spawn_right(){
    PartitionCursor* nxt = new PartitionCursor(_config);
    nxt->_previous = this;
    nxt->_next = _next;
    if(_next){
        _next->_previous = nxt;
    }
    _next = nxt;
    _next->reposition();
    _next->reindex();
    return nxt;
}
PartitionCursor *PartitionCursor::spawn_left(){
    if(_previous){
        return _previous->spawn_right();
    }
    PartitionCursor* prev = new PartitionCursor(_config);
    prev->_next = this;
    prev->_previous = _previous;
    _previous = prev; // _previous is expected to be null at this point
    _previous->reposition();
    _previous->reindex();
    return prev;
}

void PartitionCursor::reposition(){
    if(_type == RegularCursor){
        int nxt = _next ? _next->_shift : width();
        int prv = _previous ? _previous->_shift : 0;
        _shift = prv + (nxt - prv)/2;
    }
    if(_next) _next->reposition();
}
void PartitionCursor::reindex(){
    int prv = _previous ? _previous->_index : 0;
    _index = prv +1;
    if(_next) _next->reindex();
}
QPolygonF PartitionCursor::stick() const{
    QPolygonF polygon;
    polygon << QPointF(_shift, margin()) << QPointF(_shift, height()-stretch()) << QPointF(_shift-stretch(), height()) << QPointF(_shift+stretch(), height()) << QPointF(_shift, height()-stretch());
    return polygon;
}
bool PartitionCursor::contains(const QPointF& p) const{
    QPolygonF polygon;
    polygon << QPointF(_shift, height()-stretch()) << QPointF(_shift-stretch(), height()) << QPointF(_shift+stretch(), height()) << QPointF(_shift, height()-stretch());
    return polygon.containsPoint(p, Qt::WindingFill);
}
void PartitionCursor::draw(QPainter& painter) const{
    QBrush brush(_activated ? Qt::black : Qt::white, Qt::SolidPattern);
    QPolygonF polygon = stick();
    painter.save();
    painter.setBrush(brush);
    painter.drawPolygon(polygon);
    painter.restore();
    QRectF index_rect(QPointF(_shift-stretch(), height()-stretch()), QSizeF(2*stretch(), 2*stretch()));
    QPen pen(!_activated ? Qt::black : Qt::white);
    painter.save();
    painter.setPen(pen);
    painter.drawText(index_rect, Qt::AlignHCenter | Qt::AlignTop, QString::number(_index));
    painter.restore();
    if(_previous && !_selected && !_previous->_selected){
        painter.save();
        QRectF weight_rect(QPointF(_previous->_shift, weight()), QPointF(_shift, height()-stretch()));
        painter.setBrush(QBrush(Qt::white, Qt::SolidPattern));
        painter.drawRect(weight_rect);

        QLineF bar(QPointF(_previous->_shift, weight()), QPointF(_shift, weight()));
        QPointF mid = bar.pointAt(0.5f);
        QRectF label_rect(mid - QPointF(20,10), QSizeF(40.0f, 20));
        double w = scale(0.0f, 1.0f).to(0.0f, _config.ymax).at(_weight);
        if(activity() == PartitionConfig::WeightActivity || activity() == PartitionConfig::YValueActivity){
            painter.save();
            painter.setPen(QPen(Qt::white));
            painter.fillRect(label_rect, QBrush(Qt::black, Qt::SolidPattern));
            painter.drawText(label_rect, Qt::AlignCenter, QString::number(w, 'f', 2));
            painter.restore();
        }else if(activity() == PartitionConfig::RangeActivity && _type == RegularCursor){
            QPointF mid(_shift, stretch());
            QRectF label_rect(mid - QPointF(20,10), QSizeF(40.0f, 20));
            painter.drawRoundedRect(label_rect, 5, 5);
            double s = _config.to_range(_shift, Qt::Horizontal);
            painter.drawText(label_rect, Qt::AlignCenter, QString::number(s, 'f', 2));

            painter.drawRoundedRect(label_rect, 5, 5);
            painter.drawText(label_rect, Qt::AlignCenter, QString::number(w, 'f', 2));
        }else{
            painter.drawRoundedRect(label_rect, 5, 5);
            painter.drawText(label_rect, Qt::AlignCenter, QString::number(w, 'f', 2));
        }
        painter.restore();
    }
    if(_selected){
        double size = 20.0f;

        QPolygonF left_arrow, right_arrow;
        left_arrow  << QPointF(_shift+size/2, margin()+(height()-stretch()-margin())/2.0f)
                    << QPointF(_shift+size/2, margin()+(height()-stretch()-margin())/2.0f+size)
                    << QPointF(_shift+size/2+size, margin()+(height()-stretch()-margin())/2.0f+size/2.0f);

        right_arrow << QPointF(_shift-size/2, margin()+(height()-stretch()-margin())/2.0f)
                    << QPointF(_shift-size/2, margin()+(height()-stretch()-margin())/2.0f+size)
                    << QPointF(_shift-size/2-size, margin()+(height()-stretch()-margin())/2.0f+size/2.0f);

        painter.save();
        painter.setBrush(QBrush(Qt::black, Qt::SolidPattern));
        painter.drawConvexPolygon(left_arrow);
        painter.drawConvexPolygon(right_arrow);
        painter.restore();
    }else if(_yvalue){

    }
}
bool PartitionCursor::setShift(const QPointF& p){
    if(_type != RegularCursor || !activated()) return false;
    double left = _previous ? _previous->_shift : 0.0f;
    double right = _next ? _next->_shift : width();
    if(left < p.x() && right > p.x()){
        _shift = p.x();
        return true;
    }
    return false;
}

bool PartitionCursor::inside(const QPointF& p) const{
    return (_previous && p.x() > _previous->_shift && p.x() < _shift) && (p.y() <= height()-stretch() && p.y() >= margin());
}

bool PartitionCursor::setWeight(const QPointF& p){
    if(inside(p)){
        _weight = scale(height()-stretch(), margin()).to(0, 1).at(p.y());
        return true;
    }
    return false;
}

void PartitionCursor::setType(PartitionCursor::Type type){
    _type = type;
    if(type == OpeningCursor){
        _shift = 0;
    }else if(type == ClosingCursor){
        _shift = width();
    }
}

/**
 * refer to Issue #1 board work for references
 * @brief PartitionCursor::rescale
 * @param size
 */
void PartitionCursor::rescale(const QSizeF& size){
    double wratio = size.width()/width();
    _shift = _shift*wratio;

}
bool PartitionCursor::activate(const QPointF &p){
    _activated = contains(p);
    return _activated;
}
void PartitionCursor::deactivate(bool flag){
    _activated = !flag;
}
bool PartitionCursor::activated() const{
    return _activated;
}

double PartitionCursor::weight() const {
    return scale(0.0f, 1.0f).to(height()-stretch(), margin()).at(_weight);
}
