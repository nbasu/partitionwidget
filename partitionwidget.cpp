#include "partitionwidget.h"
#include <QPainter>
#include "partitionconfig.h"
#include <QDebug>

PartitionWidget::PartitionWidget(QWidget *parent): QWidget(parent), _config(this), _first(new PartitionCursor(_config, PartitionCursor::OpeningCursor)){
    _last = _first->spawn_right();
    _last->setType(PartitionCursor::ClosingCursor);
    _cursors << _first << _last;
    setFocusPolicy(Qt::StrongFocus);
}

PartitionWidget::~PartitionWidget(){

}

void PartitionWidget::paintEvent(QPaintEvent *ev){
    Q_UNUSED(ev);
    QPainter painter(this);
//    for(PartitionCursor* cursor: _cursors){
//        cursor->draw(painter);
//        qDebug() << "PartitionWidget::paintEvent: " << cursor->_index;
//    }
    PartitionCursor* c = _first;
    while(c){
        c->draw(painter);
        c = c->_next;
    }
}

void PartitionWidget::mousePressEvent(QMouseEvent *ev){
    if(activity() == PartitionConfig::YValueActivity)return;
    if(activity() == PartitionConfig::ControlActivity){
        double size = 20.0f;
        QRectF right(QPointF(focused()->_shift,      margin()+(height()-stretch()-margin())/2.0f), QSizeF(size, size));
        QRectF left (QPointF(focused()->_shift-size, margin()+(height()-stretch()-margin())/2.0f), QSizeF(size, size));
        if(left.contains(ev->pos())){
            _cursors << focused()->spawn_left();
        }else if(right.contains(ev->pos())){
            _cursors << focused()->spawn_right();
        }else{
            focused()->_selected = false;
            _config.activity = PartitionConfig::NoActivity;
            _config.focused = 0x0;
        }
    }else{
        for(PartitionCursor* cursor: _cursors){
            bool activated = cursor->activate(ev->pos());
            if(activated && cursor->_type == PartitionCursor::RegularCursor){
                _config.focused = cursor;
                _config.activity = PartitionConfig::RangeActivity;
                setCursor(Qt::SizeHorCursor);
                break;
            }else if(cursor->inside(ev->pos())){
                _config.focused = cursor;
                _config.activity = PartitionConfig::WeightActivity;
                setCursor(Qt::SizeVerCursor);
            }
        }
    }
    update();
}

void PartitionWidget::mouseReleaseEvent(QMouseEvent *ev){
    Q_UNUSED(ev);
    if(activity() == PartitionConfig::ControlActivity || activity() == PartitionConfig::YValueActivity)return;
    setCursor(Qt::ArrowCursor);
    if(!focused()) return;
    focused()->deactivate();
    _config.focused = 0x0;
    _config.activity = PartitionConfig::NoActivity;
    update();
}

void PartitionWidget::mouseMoveEvent(QMouseEvent *ev){
    if(activity() == PartitionConfig::ControlActivity || activity() == PartitionConfig::YValueActivity){
        return;
    }
    if(!focused()) return;
    if(activity() == PartitionConfig::RangeActivity){
        focused()->setShift(ev->pos());
    }else if(activity() == PartitionConfig::WeightActivity){
        focused()->setWeight(ev->pos());
    }
    update();
}

void PartitionWidget::mouseDoubleClickEvent(QMouseEvent *ev){
    if(activity() == PartitionConfig::ControlActivity){return;}
    for(PartitionCursor* cursor: _cursors){
        if(cursor->contains(ev->pos())){
            _config.activity = PartitionConfig::ControlActivity;
            _config.focused = cursor;
            cursor->_selected = true;
            update();
            break;
        }else if(cursor->inside(ev->pos())){
            qDebug() << "PartitionWidget::mouseDoubleClickEvent: " << cursor->_weight;
            _config.activity = PartitionConfig::YValueActivity;
            _config.focused = cursor;
            cursor->_yvalue = true;
            update();
            break;
        }
    }
}

void PartitionWidget::resizeEvent(QResizeEvent *ev){
    Q_UNUSED(ev);
    QSize sz = size();
    for(PartitionCursor* cursor: _cursors){
        cursor->rescale(sz);
    }
    _config.height = size().height();
    _config.width  = size().width();
    update();
}

void PartitionWidget::keyReleaseEvent(QKeyEvent *ev){
    if(activity() == PartitionConfig::YValueActivity){
        if(ev->key() != Qt::Key_Return && ev->key() != Qt::Key_Enter){
            focused()->weight_label += ev->text();
            bool okay;
            double new_max = focused()->weight_label.toDouble(&okay);
            if(okay){
                _config.ymax = new_max/focused()->_weight;
            }
        }else{
            focused()->weight_label.clear();
            focused()->_yvalue = false;
            _config.activity = PartitionConfig::NoActivity;
            _config.focused = 0x0;
        }
    }
    update();
}

void PartitionWidget::setLimit(double value, Qt::Orientation orientation, bool cascade){
    if(orientation == Qt::Horizontal){
        _config.xmax = value;
    }else if(orientation == Qt::Vertical){
        if(cascade){
            double ratio = value / _config.ymax;
            for(PartitionCursor* c = _first->_next; c != 0x0; c = c->_next){
                double backup = c->_weight;
                double visual_value_previous = scale((double)0.0f, (double)1.0f).to(0.0f, _config.ymax).at(c->_weight);
                double actual_value_present  = scale((double)0.0f, value).to(0.0f, 1.0f).at(visual_value_previous);
//                double actual_value = scale((double)0.0f, value).to(0.0f, 1.f).at(visual_value_present);
                c->_weight = actual_value_present;
                qDebug() << __LINE__ << _config.ymax << value << c->_index << backup << c->_weight;
            }
        }
        _config.ymax = value;
    }
    update();
}

void PartitionWidget::insert(unsigned index, double delta, double value){
    PartitionCursor* cursor = _first;
    while((cursor++)->_index != index && cursor){}
    if(cursor){
        PartitionCursor* n = cursor->spawn_right();
        n->_shift = cursor->_shift+delta;
        n->_weight = value;
    }
    update();
}

void PartitionWidget::push(double delta, double value){
    PartitionCursor* spawned = _last->spawn_left();
    {// compute shift from delta
        double e = _config.xmax;
        double ep = _last->_shift;
        double v = delta;
        double vp = ep * v / e;
        spawned->_shift = spawned->_previous->_shift + vp;
    }
    {// compute weight from value
        PartitionCursor* cursor = spawned;
        cursor->_weight = scale((double)0.0f, _config.ymax).to(0.0f, 1.0f).at(value);
        qDebug() << "cursor->_weight: " << cursor->_weight;
//        cursor = cursor->_previous;
//        while(cursor){
//            cursor->_weight = scale(0.0f, 1.0f).to(0.0f, 1.0f).at(cursor->_weight);
//            cursor = cursor->_previous;
//        }
    }
    _cursors << spawned;
    update();
}

void PartitionWidget::clear(){
    QVector<PartitionCursor*> to_be_deleted;
    for(PartitionCursor* cursor: _cursors){
        if(_first != cursor && _last != cursor){
            to_be_deleted << cursor;
        }
    }
    for(PartitionCursor* cursor: to_be_deleted){
        if(_cursors.removeOne(cursor)) {
            delete cursor;
            cursor = 0x0;
        }
    }
    _first->_next = _last;
    _last->_previous = _first;
    update();
}

void PartitionWidget::setValue(unsigned int index, double value){
    PartitionCursor* spawned = _first;
    while(spawned){
        if(spawned->_index == index)
            break;
        spawned = spawned->_next;
    }
    if(spawned){// compute weight from value
        PartitionCursor* cursor = spawned;
        cursor->_weight = scale((double)0.0f, _config.ymax).to(0.0f, 1.0f).at(value);
//        cursor = cursor->_previous;
//        while(cursor){
//            cursor->_weight = scale((double)0.0f, _config.ymax).to(0.0f, 1.0f).at(cursor->_weight);
//            cursor = cursor->_previous;
//        }
    }
    update();
}
