#include "partitionconfig.h"
#include <functional>
#include <QDebug>

PartitionConfig::PartitionConfig(QWidget* widget): width(widget->width()), height(widget->height()), stretch_min(15.0f), margin(15.0f), xmax(1.0f), ymax(1.0f), activity(NoActivity), focused(0x0){

}

double PartitionConfig::stretch() const{
    return std::min(stretch_min, (double)height/8.0f);
}

double PartitionConfig::canvash(double max) const{
    double b = margin;
    double e = (max ? max : height)-stretch();
    return e - b;
}

double PartitionConfig::canvasw() const{
    return width;
}

double PartitionConfig::to_domain(double value, Qt::Orientation orientation) const{
    if(orientation == Qt::Horizontal){
        return 1.0f - (width - value)/canvasw();
    }else if(orientation == Qt::Vertical){
        return (height-stretch() - value)/canvash();
    }
    return -1.0f;
}

double PartitionConfig::to_range(double value, Qt::Orientation orientation) const{
    double v = to_domain(value, orientation);
    if(orientation == Qt::Vertical){
        double b  = 0;
        double bp = 0;
        double e  = 1.0f;
        double ep = ymax;
        // double vp = (bp*e -b*ep -bp*v +ep*v)/(e-b);
        double vp = ep*v;
        Q_UNUSED(b);
        Q_UNUSED(e);
        Q_UNUSED(bp);
        return vp;
    }else if(orientation == Qt::Horizontal){
        double delta_dashed = xmax;
        return (delta_dashed*v);
    }
    return -1.0f;
}
