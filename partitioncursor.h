#ifndef PARTITIONCURSOR_H
#define PARTITIONCURSOR_H

#include <QPainter>
#include "partitionconfig.h"

class PartitionWidget;

class Q_DECL_EXPORT PartitionCursor{
  friend class PartitionWidget;
  const PartitionConfig& _config;
  public:
    enum Type{
        RegularCursor,
        OpeningCursor,
        ClosingCursor
    };

    Type     _type;     /// Opening or Closing or Regular Cursor

    unsigned _index;    /// index of a cursor
    double   _shift;    /// horizontal drag value (in pixels from top)
    double   _weight;   /// vertical drag value (in pixels fro left)

    PartitionCursor* _previous;     /// previous cursor (null for the opening cursor)
    PartitionCursor* _next;         /// next cursor (null for the closing cursor)

    bool     _activated;    /// cursor activated when being dragged (between mouse press and release)
    bool     _selected;     /// cursor selected when double clicked
    bool     _yvalue;       /// cursor value area double clicked

    /**
     * @brief returns a polygon of the shape of the curshor stick with the triangle
     * @param height window height
     * @return PolygonF
     */
    QPolygonF stick() const;
    /**
     * @brief checks whether the given point is inside the cursor handle triangle or not
     * @param p
     * @return boolean
     */
    bool contains(const QPointF& p) const;
    /**
     * @brief draw's the cursor with the given painter
     * @param painter
     * @param height
     */
    void draw(QPainter& painter) const;
    /**
     * @brief shift's the cursor to the new position unless it overlaps with its previous or next cursor. requires re draw if returns true
     * @param p
     * @return boolean
     */
    bool setShift(const QPointF& p);
    /**
     * @brief inside the partition weight rectangle area
     * @param p
     * @return
     */
    bool inside(const QPointF& p) const;
    bool setWeight(const QPointF& p);
    double weight() const;
    void setType(Type type);
    void rescale(const QSizeF& size);
    bool activate(const QPointF& p);
    void deactivate(bool flag = true);
    bool activated() const;
  private:
    PartitionCursor(const PartitionConfig& config, Type type = RegularCursor);
    PartitionCursor* spawn_right();
    PartitionCursor* spawn_left();
    void reposition();
    void reindex();
  public:
    double height() const  {return _config.height;}
    double width() const   {return _config.width;}
    double stretch() const {return _config.stretch();}
    double margin() const  {return _config.margin;}
    PartitionConfig::Activity activity() const {return _config.activity;}
  public:
    double value() const {return scale(0.0f, 1.0f).to(0.0f, _config.ymax).at(_weight);}
    double position() const {return _config.to_range(_shift, Qt::Horizontal);}
    std::pair<double, double> range() const{return std::make_pair(_previous ? _previous->position() : 0, position());}
  private:
    QString weight_label;
};

#endif // PARTITIONCURSOR_H
